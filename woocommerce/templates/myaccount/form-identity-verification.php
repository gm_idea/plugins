<?php
/**
 * Selfie upload form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/myaccount/form-identity-verification.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.9
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$page_title =__( 'Identity Verification', 'woocommerce' ) ;
$identity_status = intval(get_user_meta( $user->ID, 'identity_status', true));

if($identity_status == 0)
	$status_notice =( __( 'Please upload selfies for verification.', 'woocommerce' ) );
else if($identity_status == 1)
	$status_notice =( __( 'Your ID verification is in progress. Please wait for the approval.', 'woocommerce' ) );
else if($identity_status == 2)
	$status_notice =( __( 'Your ID verification is successful.', 'woocommerce' ) );
else
	$status_notice =( __( 'Your ID verification failed. Please try uploading again.', 'woocommerce' ) );
do_action( 'woocommerce_before_identity_verification_form' ); 

if($identity_status == 1)
{
?>
<fieldset>
<div class="woocommerce-message"><?php echo $status_notice;?> </div>
</fieldset>
<?php
}
else
{
?>


	<form enctype="multipart/form-data" method="post">
		<fieldset>
			<legend><?php _e( 'Upload Selfies', 'woocommerce' ); ?></legend>
			<div class="woocommerce-message"><?php echo $status_notice;?> </div>
			<div id="iv_camera"  style="display: none;">
				<video id="iv_video" ></video>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="identity_1">Selfie</label>
					<input id="iv_take_1" type="button" value="Take ID1"></input>
					<img id="iv_id_1" style="max-width:50px;"/>
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for="identity_2">Selfie with Candadian government issued ID</label>
					<input id="iv_take_2" type="button" value="Take ID2"></input>
					<img id="iv_id_2" style="max-width:50px;"/>
				</p>
				<canvas id="iv_canvas" style="display:none;" ></canvas>
				<input type="hidden" name="up_1" id="up_1" value="">
				<input type="hidden" name="up_2" id="up_2" value="">
			</div>

			<div id="iv_fallback">
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for = "identity_1"><?php _e( 'Selfie', 'woocommerce' ); ?></label>
					<input name="identity_1" id="identity_1" type="file"/>
				</p>
				<p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
					<label for = "identity_2"><?php _e( 'Selfie with Candadian government issued ID', 'woocommerce' ); ?></label>
					<input name="identity_2" id="identity_2" type="file"/>
				</p>
			</div>

		</fieldset>
		<div class="clear"></div>
		<?php do_action( 'woocommerce_identity_verification_form' ); ?>
		<?php wp_nonce_field( 'woocommerce-upload_identity' ); ?>
		<input type="submit" class="woocommerce-Button button" name="upload_identity" value="<?php esc_attr_e( 'Upload Selfies', 'woocommerce' ); ?>" />
		<input type="hidden" name="action" value="upload_identity" />
		<input type="hidden" name="iv_type" id="iv_type" value="0"/>

	</form>
<?php
}

do_action( 'woocommerce_after_identity_verification_form' ); ?>
