<?php
/**
 * Admin View: Page - Addons
 *
 * @var string $view
 * @var object $addons
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
	<div class="wrap woocommerce">
		<h1 class="screen-reader-text"><?php echo __( 'Manage Identity Verification', 'woocommerce' ); ?></h1>
			<?php
				//self::show_messages();
			?>
	</div>
