<?php
/**
 * WooCommerce Admin ID Verification Class
 *
 * @author   WooThemes
 * @category Admin
 * @package  WooCommerce/Admin
 * @version  2.5.0
 */
use Google\Cloud\ServiceBuilder;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( 'WC_Admin_ID_Verification', false ) ) :

/**
 * WC_Admin_ID_Verification Class.
 */
class WC_Admin_ID_Verification {

	/**
	 * ID Verification pages.
	 *
	 * @var array
	 */
	private static $settings = array();

	private static $timeout = 60;

	/**
	 * Error messages.
	 *
	 * @var array
	 */
	private static $errors   = array();

	/**
	 * Update messages.
	 *
	 * @var array
	 */
	private static $messages = array();


	/**
	 * Save the settings.
	 */
	public static function save() {
		global $wpdb;

		if ( empty( $_REQUEST['_wpnonce'] ) || ! wp_verify_nonce( $_REQUEST['_wpnonce'], 'woocommerce-admin-identity-verification' ) ) {
			die( __( 'Action failed. Please refresh the page and retry.', 'woocommerce' ) );
		}
		if (!$wpdb->query("SHOW TABLES LIKE '{$wpdb->prefix}woocommerce_id_log'")) 
		{
    		error_log("The table {$wpdb->prefix}woocommerce_id_log doesn't exist!");
    		die( __( "The table {$wpdb->prefix}woocommerce_id_log doesn't exist.", 'woocommerce' ) );
		}

		// Trigger actions
		do_action( 'woocommerce_id_verification_save');
		if(isset($_POST['user_id']))
		{
			$reviewer_id = get_current_user_id();
			$user_id = $_POST['user_id'];
			$user_login = $_POST['user_login'];
			if(isset($_POST['id_approve']))
			{
				update_user_meta($user_id,'identity_status',2);
				self::add_message( __( 'ID is approved for the user ', 'woocommerce' ) .$user_login );
				$wpdb->query( $wpdb->prepare( 
					"
					INSERT into {$wpdb->prefix}woocommerce_id_log
					(user_id,action,reviewer_id) VALUES(%d,%d,%d)
					",$user_id,2,$reviewer_id
				) );
			}
			else if(isset($_POST['id_request']))
			{
				update_user_meta($user_id,'identity_status',0);
				self::add_message( __( 'Requested further verification for the user ', 'woocommerce' ).$user_login );
				$wpdb->query( $wpdb->prepare( 
					"
					INSERT into {$wpdb->prefix}woocommerce_id_log
					(user_id,action,reviewer_id) VALUES(%d,%d,%d)
					",$user_id,0,$reviewer_id
				) );
			}
			else if(isset($_POST['id_decline']))
			{
				update_user_meta($user_id,'identity_status',3);
				self::add_message( __( 'ID is declined for the user ', 'woocommerce' ).$user_login );
				$wpdb->query( $wpdb->prepare( 
					"
					INSERT into {$wpdb->prefix}woocommerce_id_log
					(user_id,action,reviewer_id) VALUES(%d,%d,%d)
					",$user_id,3,$reviewer_id
				) );
			}
		}
/*		else if(isset($_POST['wc_sa_credentials']))
		{
			$wc_sa_bucket = $_POST['wc_sa_bucket'];
			$wc_sa_keyfile = $_POST['wc_sa_keyfile'];
			update_option('wc_sa_bucket',$wc_sa_bucket,'yes');
			update_option('wc_sa_keyfile',$wc_sa_keyfile,'yes');
			self::add_message( __( 'The credentials updated successfully.', 'woocommerce' ));
		}
*/


		do_action( 'woocommerce_id_verification_saved' );
	}

	/**
	 * Add a message.
	 * @param string $text
	 */
	public static function add_message( $text ) {
		self::$messages[] = $text;
	}

	/**
	 * Add an error.
	 * @param string $text
	 */
	public static function add_error( $text ) {
		self::$errors[] = $text;
	}

	/**
	 * Output messages + errors.
	 * @return string
	 */
	public static function show_messages() {
		if ( sizeof( self::$errors ) > 0 ) {
			foreach ( self::$errors as $error ) {
				echo '<div id="message" class="error inline"><p><strong>' . esc_html( $error ) . '</strong></p></div>';
			}
		} elseif ( sizeof( self::$messages ) > 0 ) {
			foreach ( self::$messages as $message ) {
				echo '<div id="message" class="updated inline"><p><strong>' . esc_html( $message ) . '</strong></p></div>';
			}
		}
	}

	/**
	 * Settings page.
	 *
	 * Handles the display of the main woocommerce settings page in admin.
	 */
	public static function output() {
		global $wpdb;

		$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		do_action( 'woocommerce_id_verification_start' );
		$c_user = wp_get_current_user();
		$role = $c_user->roles ? $c_user->roles[0] : false;
		if($role !== "administrator" && $role !== "idreviewer")
		{
			self::add_error( __('You do not have permission to access this page.',"woocommerce"));
			self::show_messages();
			return;
		}


/*		wp_enqueue_script( 'woocommerce_settings', WC()->plugin_url() . '/assets/js/admin/settings' . $suffix . '.js', array( 'jquery', 'jquery-ui-datepicker', 'jquery-ui-sortable', 'iris', 'select2' ), WC()->version, true );

		wp_localize_script( 'woocommerce_settings', 'woocommerce_settings_params', array(
			'i18n_nav_warning' => __( 'The changes you made will be lost if you navigate away from this page.', 'woocommerce' ),
		) );
*/
		
		// Save settings if data has been posted
		if ( ! empty( $_POST ) ) {
			self::save();
		}

		// Add any posted messages
		if ( ! empty( $_GET['wc_error'] ) ) {
			self::add_error( stripslashes( $_GET['wc_error'] ) );
		}

		if ( ! empty( $_GET['wc_message'] ) ) {
			self::add_message( stripslashes( $_GET['wc_message'] ) );
		}
		$wc_sa_keyfile = stripslashes(get_option("wc_sa_keyfile"));
		$wc_sa_bucket = get_option("wc_sa_bucket");



		if(isset($_GET['user_id']) && !isset($_POST['user_id']))// && !isset($_POST['wc_sa_credentials']))
		{
			$user = get_user_by('id',$_GET['user_id']);
			if(!$user)
				self::add_error(__('Can not find the user with the given ID','woocommerce'));
			else
			{
				//Add a form
				?>
				<div class="wrap woocommerce">
					<h1 class="screen-reader-text"><?php echo __( 'Manage Identity Verification for User', 'woocommerce' )." ".$user->user_login; ?></h1>
					<?php
						self::show_messages();
					?>
					<h2><?php echo __( 'Manage Identity Verification for User', 'woocommerce' )." ".$user->user_login; ?></h2>
					<table class="display form-table width="100%" cellspacing="0">
						<?php

							$status  =intval(get_user_meta($user->ID,'identity_status',true));
							$up_id_name_1 =get_user_meta($user->ID,'ID_1_name',true);
							$up_id_name_2 =get_user_meta($user->ID,'ID_2_name',true);
							//get temporary link
							$keyFile = json_decode($wc_sa_keyfile,true);
							if(!$keyFile || !$wc_sa_bucket)
							{
							    wc_add_notice( __( 'Please set the bucket credentials.', 'woocommerce' ) );
							}
							else{
								$gcloud = new ServiceBuilder([
								    'keyFile' => $keyFile,
								    'projectId' => $keyFile['project_id']
								]);
								
								$storage = $gcloud->storage();
								$bucket = $storage->bucket($wc_sa_bucket);
								if($up_id_name_1)
								{
						            $object = $bucket->object($up_id_name_1);
						            $up_id_1 = $object->signedUrl(time() + self::$timeout);
					        	}
								if($up_id_name_2)
								{
					    	        $object = $bucket->object($up_id_name_2);
					        	    $up_id_2 = $object->signedUrl(time() + self::$timeout);
					        	}
							}

							$address_1 =get_user_meta($user->ID,'billing_address_1',true);
							$address_2 =get_user_meta($user->ID,'billing_address_2',true);
							$city =get_user_meta($user->ID,'billing_city',true);
							$state =get_user_meta($user->ID,'billing_state',true);
							$postcode =get_user_meta($user->ID,'billing_postcode',true);
							$country =get_user_meta($user->ID,'billing_country',true);
							$address = apply_filters( 'woocommerce_my_account_my_address_formatted_address', array(
								'company'     => get_user_meta( $user->ID, 'billing_company', true ),
								'address_1'   => get_user_meta( $user->ID, 'billing_address_1', true ),
								'address_2'   => get_user_meta( $user->ID, 'billing_address_2', true ),
								'city'        => get_user_meta( $user->ID, 'billing_city', true ),
								'state'       => get_user_meta( $user->ID, 'billing_state', true ),
								'postcode'    => get_user_meta( $user->ID, 'billing_postcode', true ),
								'country'     => get_user_meta( $user->ID, 'billing_country', true ),
							), $user->ID, 'billing' );

							$formatted_address = WC()->countries->get_formatted_address( $address );

							$bday    =get_user_meta($user->ID,'custom_field_1',true);
							$f_name  =get_user_meta($user->ID,'first_name',true);
							$l_name  =get_user_meta($user->ID,'last_name',true);
							if($status == 0  )
								$status_value = __("Unverified","woocommerce");
							else if($status == 1)
								$status_value = __("Pending","woocommerce");
							else if($status == 2)
								$status_value = __("Approved","woocommerce");
							else if($status == 3)
								$status_value = __("Declined","woocommerce");

							?>
						<form method="post">
							<tr>
								<td>User Login</td><td><?php echo $user->user_login; ?></td>
								<td rowspan = "6" width="50%">
									<?php

										$customer_orders = get_posts( array(
										    'numberposts' => -1,
										    'meta_key'    => '_customer_user',
										    'meta_value'  => $user->ID,
										    'post_type'   => wc_get_order_types(),
										    'post_status' => array_keys( wc_get_order_statuses() ),
										) );
										//var_dump($customer_orders);
										foreach($customer_orders as $customer_order)
										{
											$order = wc_get_order($customer_order->ID);
											foreach( $order-> get_items() as $item_key => $item_values )
											{
											    $item_name = $item_values->get_name(); 
											    $item_type = $item_values->get_type(); 
										        $item_data = $item_values->get_data();
											    $product_name = $item_data['name'];
											    $product_id = $item_data['product_id'];
											    $variation_id = $item_data['variation_id'];
											    $quantity = $item_data['quantity'];
											    $tax_class = $item_data['tax_class'];
											    $line_subtotal = $item_data['subtotal'];
											    $line_subtotal_tax = $item_data['subtotal_tax'];
											    $line_total = $item_data['total'];
											    $line_total_tax = $item_data['total_tax'];
											    echo "<a href='".$order->get_view_order_url()."'>".$product_name ." X " . $quantity . "</a><br/>";
											}
										}
										echo "<br/><br/>";


										if (!$wpdb->query("SHOW TABLES LIKE '{$wpdb->prefix}woocommerce_id_log'")) 
										{
								    		error_log("The table {$wpdb->prefix}woocommerce_id_log doesn't exist!");
								    		echo ( __( "The table {$wpdb->prefix}woocommerce_id_log doesn't exist.", 'woocommerce' ) );
										}
										$logs = $wpdb->get_results( "
										SELECT * from {$wpdb->prefix}woocommerce_id_log where user_id = $user->ID;" );
/*										$orders = $wpdb->get_results( "
										SELECT * from {$wpdb->prefix}woocommerce_order_items where user_id = $user->ID;" );
*/										if($logs)
										{
											foreach($logs as $log)
											{
												$reviewer_name = "";
												if($log->reviewer_id)
													$reviewer_name = get_user_meta($log->reviewer_id,"nickname",true);
												$action = array("Requested further info by ",
													"Selfie uploaded ",
													"Approved by ",
													"Rejected by ");
												$output = $action[$log->action] . $reviewer_name . " on " . $log->timestamp;
												echo $output . "<br/>";
											}
										}
									?>
								</td>
							</tr>
							<tr>
								<td>First Name</td><td><?php echo $f_name; ?></td>
							</tr>
							<tr>
								<td>Last Name</td><td><?php echo $l_name; ?></td>
							</tr>
							<tr>
								<td>Birthday</td><td><?php echo $bday; ?></td>
							</tr>
							<tr>
								<td>Address</td><td><?php 
								if ( ! $formatted_address ) {
									_e( 'You have not set up this type of address yet.', 'woocommerce' );
								} else {
									echo $formatted_address;
								}

								?></td>
							</tr>
							<tr>
								<td>Verification Status</td>
								<td><?php echo $status_value; ?></td>
							</tr>
							<tr>
								<td>ID1</td>
								<td colspan="2">
									<?php 
									if($up_id_1) 
										echo "<img style='max-width:100%' src='$up_id_1'/>";
									else
										echo 'None';
									?>
									
								</td>
							</tr>
							<tr>
								<td>ID2</td>
								<td colspan="2">
									<?php 
									if($up_id_2) 
										echo "<img style='max-width:100%' src='$up_id_2'/>";
									else
										echo 'None';
									?>
								</td>
							</tr>

							<tr>
								<td colspan="3">
								<?php wp_nonce_field( 'woocommerce-admin-identity-verification' ); ?>
								<input type="hidden" name="user_id" value="<?php echo $user->ID; ?>"/>
								<input type="hidden" name="user_login" value="<?php echo $user->user_login; ?>"/>
								<input type="submit" class="button complete" name="id_approve" value="Approve"/>
								<input type="submit" class="button complete" name="id_request" value="Request Further Verification"/>
								<input type="submit" class="button complete" name="id_decline" value="Decline"/>
								<a class="button" href="?page=wc-id-verification">Back</a>
								</td>
							</tr>
						</form>
					</table>
				</div>

				<?php
			}

		}
		else
		{


		?>
			<div class="wrap woocommerce">
				<h1 class="screen-reader-text"><?php echo __( 'Manage Identity Verification', 'woocommerce' ); ?></h1>
				<?php
					self::show_messages();
					/*
				?>
				<h2><?php echo __( 'Manage Credentials', 'woocommerce' ); ?></h2>
				<table class="display form-table width="100%" cellspacing="0">
				<form method="post">
					<?php wp_nonce_field( 'woocommerce-admin-identity-verification' ); ?>
					<tr>
						<td width="20%">Bucket</td>
						<td ><input type="text" class="regular-text" name="wc_sa_bucket" value="<?php echo $wc_sa_bucket;?>" /></td>
					</tr>
					<tr>
						<td>Service Account JSON</td>
						<td><textarea style="width:40em;height:200px;" name="wc_sa_keyfile"><?php echo $wc_sa_keyfile;?></textarea></td>
					</tr>
					<tr>
						<td><input name="wc_sa_credentials" id="submit" class="button button-primary" value="Save Changes" type="submit"></td>
					</tr>
				</form>
				</table>
				<?php */ ?>
				<h2><?php echo __( 'Manage Identity Verification', 'woocommerce' ); ?></h2>
				<table id="id_table" class="display form-table width="100%" cellspacing="0">
					<tr>
						<td><?php echo __( 'User', 'woocommerce' ); ?></td>
						<td><?php echo __( 'Email Address', 'woocommerce' ); ?></td>
						<td><?php echo __( 'Signup Date', 'woocommerce' ); ?></td>
						<td><?php echo __( 'Status', 'woocommerce' ); ?></td>
					</tr>
					<?php
					$users = get_users( 'role=subscriber' );
					foreach ( $users as $user ) {
						$status  =intval(get_user_meta($user->ID,'identity_status',true));
						$up_id_1 =get_user_meta($user->ID,'ID_1_url',true);
						$up_id_2 =get_user_meta($user->ID,'ID_2_url',true);
						if($status == 0  )
							$status_value = __("Unverified","woocommerce");
						else if($status == 1)
							$status_value = __("Pending","woocommerce");
						else if($status == 2)
							$status_value = __("Approved","woocommerce");
						else if($status == 3)
							$status_value = __("Declined","woocommerce");

						?>
					<tr >
						<form method="post" >

						<td><a href="?page=wc-id-verification&user_id=<?php echo $user->ID;?>"><?php echo $user->user_login; ?></a></td>
						<td><a href="?page=wc-id-verification&user_id=<?php echo $user->ID;?>"><?php echo $user->user_email; ?></a></td>
						<td><a href="?page=wc-id-verification&user_id=<?php echo $user->ID;?>"><?php echo $user->user_registered; ?></a></td>
						<td><?php echo $status_value; ?></td>
						</form>
					</tr>
					<?php } ?>
				</table>
			</div>
		<?php
		}
	}



}

endif;
