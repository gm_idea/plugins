<?php
error_reporting(E_ALL);
require_once 'vendor/autoload.php';
use Google\Cloud\ServiceBuilder;
$gcloud = new ServiceBuilder([
    'keyFile' => json_decode(file_get_contents('private/service-account.json'), true),
    'projectId' => 'inner-synapse-169420'
]);
echo "<br/>4";
// Fetch an instance of the Storage object
//var_dump($gcloud);
$storage = $gcloud->storage();
$bucket = $storage->bucket('bw-media-store-bucket');
//var_dump($bucket);
// Check file submission 
if (!isset($_FILES['uploadfile']) || !is_uploaded_file($_FILES['uploadfile']['tmp_name'])) {
    // not sent - nothing
    $upload_message = "waiting a file";
}else{
    // Upload a file to the bucket
    $file_to_upload = $_FILES['uploadfile']['tmp_name'];
        if($result = $bucket->upload(fopen($_FILES['uploadfile']['tmp_name'], 'r'),
            [
            'name' => $_FILES['uploadfile']['name']
            ]))
        {
            $upload_message = "Upload Done!";
            $object = $bucket->object($_FILES['uploadfile']['name']);
            $object->downloadToFile($_FILES['uploadfile']['name']);
        }
        else
            $upload_message = "Upload Failed!";
}


?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">

  <title>Google Storage Upload File</title>
  <meta name="description" content="Google Storage Upload File">
  <meta name="author" content="Fabio Ferrari">

</head>
    <body>
        <form action="index.php" method="post" enctype="multipart/form-data">
            Select file to upload:
            <input type="file" name="uploadfile" id="uploadfile"><br />
            <input type="submit" value="Upload File" name="submit"><br />
        </form>
        <div>Result message: <?php echo $upload_message ?></div>
    </body>
</html>