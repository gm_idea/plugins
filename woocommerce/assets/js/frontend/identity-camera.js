/* Configure a few settings and attach camera */
//var constraints = { audio: false, video: { width: 1920, height: 1200 } }; 
var constraints = {
		audio: false,
		video: {
			optional: [
				{minWidth: 320},
				{minWidth: 640},
				{minWidth: 800},
				{minWidth: 900},
				{minWidth: 1024},
				{minWidth: 1280},
				{minWidth: 1920},
				{minWidth: 2560}
			]
		}
	};

navigator.mediaDevices.getUserMedia(constraints)
.then(function(mediaStream) {
		var video = document.getElementById("iv_video");
		var canvas = document.getElementById("iv_canvas");
		var iv_take_1 = document.getElementById("iv_take_1");
		var iv_take_2 = document.getElementById("iv_take_2");
		var iv_camera=document.getElementById("iv_camera");
		var iv_fallback=document.getElementById("iv_fallback");
		video.srcObject = mediaStream;
		video.onloadedmetadata = function(e) {
			iv_fallback.style.display = "none";
			iv_camera.style.display = "inline";
			document.getElementById("iv_type").value = 1;

		    video.play();
		    //img.width = video.width;
		    var width = video.videoWidth
		    , height = video.videoHeight;
		      canvas.width = width;
		      canvas.height = height;

		    iv_take_1.onclick = function() {
					canvas.getContext("2d").drawImage(video,0,0);
					var img = document.getElementById("iv_id_1");
					img.src = canvas.toDataURL('image/jpeg');
					document.getElementById("up_1").value = img.src;

				};
		    iv_take_2.onclick = function() {
					canvas.getContext("2d").drawImage(video,0,0);
					var img = document.getElementById("iv_id_2");
					img.src = canvas.toDataURL('image/jpeg');
					document.getElementById("up_2").value = img.src;

				};
		  };
})
.catch(function(err) { console.log(err.name + ": " + err.message); }); // always check for errors at the end.
